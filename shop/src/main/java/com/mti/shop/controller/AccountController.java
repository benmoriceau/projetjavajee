package com.mti.shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mti.shop.model.Account;
import com.mti.shop.model.Type;
import com.mti.shop.service.AccountService;
import com.mti.shop.service.TypeService;
import com.mti.shop.tool.GlobalSessionElement;
import com.mti.shop.tool.SessionTool;

@Controller
@RequestMapping(value = "/account")
public class AccountController {
	@Autowired
	AccountService accountService;
	
	@Autowired
	TypeService typeService;
	
	@Autowired
	GlobalSessionElement globalSessionElement;
	
	/**
	 * Method use to create an account (= edit an account with no post data)
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String createNewAccount(Model model) {
		Account curAccount = SessionTool.getAccount();
		if (null == curAccount) {
			model.addAttribute("account", new Account());
		} else {
			model.addAttribute("account", curAccount);
		}
		return "account/edit";
	}


	@RequestMapping(value = "/saveaccount", method = RequestMethod.POST)
	public String saveAccount(Model model,
			@ModelAttribute(value = "account") Account account) {
		List<Type> types = globalSessionElement.getTypes();
		
		for (Type type : types) {
			if (type.getTypeLabel().equals("ROLE_USER")) {
				accountService.saveOrUpdateAccount(account, type);
			}
		}
		

		return "account/created";
	}
}
