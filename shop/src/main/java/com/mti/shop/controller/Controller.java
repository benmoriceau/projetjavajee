package com.mti.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mti.shop.service.ShopService;


@org.springframework.stereotype.Controller
public class Controller {
	
	private ShopService shopService;
	
	@Autowired	
	public void setShopService(ShopService shopService) {
		this.shopService = shopService;
	}



	@RequestMapping(value="/newshop", method = RequestMethod.GET)
	public String shopAdd() {
		
		ModelAndView model = new ModelAndView("addShop");
		model.addObject("shop", shopService.getAllShop());
		model.addObject("msg", "hello world");

		return "addShop";
	}
}
