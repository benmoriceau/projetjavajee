package com.mti.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mti.shop.dao.AccountDao;
import com.mti.shop.service.AccountService;
import com.mti.shop.tool.SessionTool;


@Controller
public class LoginController {

	
	@Autowired
	AccountService aSev;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String handleLogin() {
		return "login/login";
	}
	
	@RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
	public String handleLoginFailure() {
		return "login/loginfailed";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String handleLogout() {
		return "login/logout";
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String afterConnection(Model model) {
		model.addAttribute("account", SessionTool.getAccount());
		
		return "welcome/homeBasicUser";
	}
}
