package com.mti.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mti.shop.model.Shop;
import com.mti.shop.service.CategorieService;
import com.mti.shop.service.CountryService;
import com.mti.shop.service.ShopService;
import com.mti.shop.tool.SessionTool;

@Controller
@RequestMapping(value = "/shop")
public class ShopController {
	@Autowired
	ShopService shopService;

	@Autowired
	CountryService countryService;

	@Autowired
	CategorieService categorieService;

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String createShop(Model model) {
		model.addAttribute("account", SessionTool.getAccount());
		model.addAttribute("shop", new Shop());
		model.addAttribute("countryList", countryService.findAll());
		model.addAttribute("categorieList", categorieService.findAll());
		return "shop/edit";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String editShop(Model model) {
		model.addAttribute("account", SessionTool.getAccount());
		model.addAttribute("shop", shopService.getCurrentShop());
		model.addAttribute("countryList", countryService.findAll());
		model.addAttribute("categorieList", categorieService.findAll());
		return "shop/edit";
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createShopPost(Model model, @ModelAttribute("shop") Shop shop) {
		shop.setCategorie(categorieService.findById(Long.valueOf(shop
				.getCatId())));
		shop.setCountry(countryService.findById(Long.valueOf(shop.getCntId())));
		shopService.addNewShopToCurrentAccount(shop);

		model.addAttribute("account", SessionTool.getAccount());
		model.addAttribute("shop", shop);
		return "shop/created";
	}
}
