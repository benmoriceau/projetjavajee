package com.mti.shop.dao;

import com.mti.shop.model.Account;

public interface AccountDao extends GenericDao<Account, Long> {
	Account getAccountByEmail(String email);
}
