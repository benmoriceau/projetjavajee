package com.mti.shop.dao;

import java.util.List;

import com.mti.shop.model.Categorie;

public interface CategorieDao extends GenericDao<Categorie, Long> {
	List<Categorie> findAll();
	Categorie findByLabel(String label);
}
