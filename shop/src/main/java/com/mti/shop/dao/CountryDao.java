package com.mti.shop.dao;

import java.util.List;

import com.mti.shop.model.Country;

public interface CountryDao extends GenericDao<Country, Long> {
	List<Country> findAll();
	Country findByLabel(String label);
}
