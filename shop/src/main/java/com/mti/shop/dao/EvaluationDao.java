package com.mti.shop.dao;

import java.util.List;

import com.mti.shop.model.Evaluation;

public interface EvaluationDao extends GenericDao<Evaluation, Long> {
	List<Evaluation> findEvaluationForAccount(int index, int offset, String login);
	List<Evaluation> findEvaluationForShop(int index, int offset, Long shopId);
	List<Evaluation> findEvaluated(int index, int offset, String login);
	List findBestEvaluatedShop(int index, int offset);
}
