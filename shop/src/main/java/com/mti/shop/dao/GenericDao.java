package com.mti.shop.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

public interface GenericDao<T, PK extends Serializable> {
	void saveOrUpdate(T instance);
	void delete(T instance);
	List<T> getByCriteria(DetachedCriteria crit);
	List<T> getByCriteria(int index, int offset, DetachedCriteria crit);
	T findById(PK id);
}
