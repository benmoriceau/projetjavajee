package com.mti.shop.dao;

import java.util.List;

import com.mti.shop.model.Product;

public interface ProductDao extends GenericDao<Product, Long> {
	List<Product> getLastProducs(int index, int offset);

	List<Product> getBestEvalProducts(int index, int offset);

	List<Product> findLastProductForShop(Integer index, Integer offset,
			String shopName);

	List<Product> findProductByCategorie(Integer index, Integer offset,
			String categorie);

	List<Product> findExpireSoonProducts(Integer index, Integer offset);

	List<Product> findProductByCountry(Integer index, Integer offset,
			String country);

	List<Product> findProductByKey(Integer index, Integer offset, String key);
}
