package com.mti.shop.dao;

import java.util.List;

import com.mti.shop.model.Shop;

public interface ShopDao extends GenericDao<Shop, Long>{
	List<Shop> getAllShop();
}
