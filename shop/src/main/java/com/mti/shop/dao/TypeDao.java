package com.mti.shop.dao;

import com.mti.shop.model.Type;

public interface TypeDao extends GenericDao<Type, Long>{
	Type findByLabel(String label);	
}
