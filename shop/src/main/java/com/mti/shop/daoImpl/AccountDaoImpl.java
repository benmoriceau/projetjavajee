package com.mti.shop.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mti.shop.dao.AccountDao;
import com.mti.shop.model.Account;

@Repository
public class AccountDaoImpl extends GenericDaoImpl<Account, Long> implements AccountDao {

	
	public AccountDaoImpl() {
		super();
		this.type = Account.class;
	}

	@Override
	public Account getAccountByEmail(String email) {
		//em.clear();
		DetachedCriteria crit = DetachedCriteria.forClass(Account.class);
		crit.add(Restrictions.eq("cptEmail", email));
		List<Account> res = getByCriteria(crit);
		return res.size() == 0 ? null : res.get(0);
	}

}
