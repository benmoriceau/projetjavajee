package com.mti.shop.daoImpl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mti.shop.dao.CategorieDao;
import com.mti.shop.model.Categorie;

@Repository
public class CategorieDaoImpl extends GenericDaoImpl<Categorie, Long> implements
		CategorieDao {
	public CategorieDaoImpl() {
		super();
		this.type = Categorie.class;
	}

	@Override
	public List<Categorie> findAll() {
		DetachedCriteria crit = DetachedCriteria.forClass(Categorie.class);
		return getByCriteria(crit);
	}

	@Override
	public Categorie findByLabel(String label) {
		DetachedCriteria crit = DetachedCriteria.forClass(Categorie.class)
				.add(Restrictions.eq("catLabel", label));
		return getByCriteria(crit).get(0);
	}
}
