package com.mti.shop.daoImpl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mti.shop.dao.CountryDao;
import com.mti.shop.model.Country;

@Repository
public class CountryDaoImpl extends GenericDaoImpl<Country, Long> implements
		CountryDao {
	public CountryDaoImpl() {
		super();
		this.type = Country.class;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Country> findAll() {
		DetachedCriteria crit = DetachedCriteria.forClass(Country.class);
		return getByCriteria(crit);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Country findByLabel(String label) {
		DetachedCriteria crit = DetachedCriteria.forClass(Country.class)
				.add(Restrictions.eq("cntLabel", label));
		
		return getByCriteria(crit).get(0);
	}
}
