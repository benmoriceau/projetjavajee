package com.mti.shop.daoImpl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mti.shop.dao.EvaluationDao;
import com.mti.shop.model.Evaluation;

@Repository
public class EvaluationDaoImpl extends GenericDaoImpl<Evaluation, Long>  implements EvaluationDao{
	public EvaluationDaoImpl() {
		super();
		this.type = Evaluation.class;
	}

	@Override
	public List findBestEvaluatedShop(int index, int offset) {
		DetachedCriteria crit = DetachedCriteria.forClass(Evaluation.class)
				.add(Restrictions.isNotNull("shopEvaluated"))
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("shopEvaluated"))
						.add(Projections.avg("evalNote"), "avgEvalNote"))
				.addOrder(Order.desc("avgEvalNote"));
		
		return getByCriteria(index, offset, crit);
	}

	@Override
	public List<Evaluation> findEvaluationForAccount(int index, int offset,
			String login) {
		DetachedCriteria crit = DetachedCriteria.forClass(Evaluation.class)
				.createAlias("evaluated", "acc")
				.add(Restrictions.eq("acc.cptEmail", login));
		
		return getByCriteria(index, offset, crit);
	}

	@Override
	public List<Evaluation> findEvaluationForShop(int index, int offset,
			Long shopId) {
		DetachedCriteria crit = DetachedCriteria.forClass(Evaluation.class)
				.createAlias("shopEvaluated", "sp")
				.add(Restrictions.eq("sp.id", shopId));
		
		return getByCriteria(index, offset, crit);
		}

	@Override
	public List<Evaluation> findEvaluated(int index, int offset, String login) {
		DetachedCriteria crit = DetachedCriteria.forClass(Evaluation.class)
				.createAlias("evaluator", "acc")
				.add(Restrictions.eq("acc.cptEmail", login));
		
		return getByCriteria(index, offset, crit);
	}
}
