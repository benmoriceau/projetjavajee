package com.mti.shop.daoImpl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;

import com.mti.shop.dao.GenericDao;

public class GenericDaoImpl<T, PK extends Serializable> implements
		GenericDao<T, PK> {

	Class<T> type;

	@Autowired
	private SessionFactory sessionFactory;

	public GenericDaoImpl() {
	}

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;

	}

	public T findById(PK id) {
		return (T) getSession().get(type, id);
	}

	@Override
	public void saveOrUpdate(T instance) {
		getSession().saveOrUpdate(instance);
	}

	@Override
	public void delete(T instance) {
		getSession().delete(instance);
	}

	@Override
	public List<T> getByCriteria(DetachedCriteria crit) {
		return crit.getExecutableCriteria(getSession()).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getByCriteria(int index, int offset, DetachedCriteria crit) {
		return crit.getExecutableCriteria(getSession())
				.setFirstResult(index * offset).setMaxResults(offset).list();
	}

}
