package com.mti.shop.daoImpl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mti.shop.dao.ProductDao;
import com.mti.shop.model.Product;

@Repository
public class ProductDaoImpl extends GenericDaoImpl<Product, Long> implements
		ProductDao {
	public ProductDaoImpl() {
		super();
		this.type = Product.class;
	}

	public List<Product> getLastProducs(int index, int offset) {
		DetachedCriteria crit = DetachedCriteria.forClass(Product.class)
				.addOrder(Order.desc("prodDatePublish"));

		List<Product> res = getByCriteria(index, offset, crit);
		return res;
	}

	public List<Product> getBestEvalProducts(int index, int offset) {
		DetachedCriteria crit = DetachedCriteria
				.forClass(Product.class)
				.createCriteria("shop", "sp")
				.createCriteria("evaluations", "eval")
				.setProjection(
						Projections.projectionList().add(
								Projections.avg("eval.evalNote"), "note"))
				.addOrder(Order.desc("note"));
		return getByCriteria(index, offset, crit);
	}

	@Override
	public List<Product> findLastProductForShop(Integer index, Integer offset,
			String shopName) {
		DetachedCriteria crit = DetachedCriteria
				.forClass(Product.class)
				.createAlias("shop", "sp")
				.add(Restrictions.ilike("sp.shopName", shopName,
						MatchMode.ANYWHERE))
				.addOrder(Order.desc("prodDatePublish"));
		return getByCriteria(index, offset, crit);
	}

	@Override
	public List<Product> findProductByCategorie(Integer index, Integer offset,
			String categorie) {
		DetachedCriteria crit = DetachedCriteria
				.forClass(Product.class)
				.createAlias("categorie", "cat")
				.add(Restrictions.ilike("cat.catLabel", categorie,
						MatchMode.ANYWHERE))
				.addOrder(Order.desc("prodDatePublish"));
		return getByCriteria(index, offset, crit);
	}

	@Override
	public List<Product> findExpireSoonProducts(Integer index, Integer offset) {
		Date yesterday = null;
		Date tomorrow = null;
		Calendar calendar = Calendar.getInstance();

		calendar.set(
				Calendar.DAY_OF_YEAR,
				calendar.get(Calendar.DAY_OF_YEAR) - 1 < calendar
						.getMinimum(Calendar.DAY_OF_YEAR) ? calendar
						.getMinimum(Calendar.DAY_OF_YEAR) : calendar
						.get(Calendar.DAY_OF_YEAR) - 1);
		yesterday = calendar.getTime();
		calendar.set(
				Calendar.DAY_OF_YEAR,
				calendar.get(Calendar.DAY_OF_YEAR) + 2 > calendar
						.getMaximum(Calendar.DAY_OF_YEAR) ? calendar
						.getMaximum(Calendar.DAY_OF_YEAR) : calendar
						.get(Calendar.DAY_OF_YEAR) + 2);
		tomorrow = calendar.getTime();

		DetachedCriteria crit = DetachedCriteria.forClass(Product.class)
				.add(Restrictions.between("prodExpire", yesterday, tomorrow))
				.addOrder(Order.desc("prodDatePublish"));

		return getByCriteria(index, offset, crit);
	}

	@Override
	public List<Product> findProductByCountry(Integer index, Integer offset,
			String country) {
		DetachedCriteria crit = DetachedCriteria
				.forClass(Product.class)
				.createAlias("shop", "sp")
				.createAlias("sp.country", "cnt")
				.add(Restrictions.ilike("cnt.cntLabel", country,
						MatchMode.ANYWHERE))
				.addOrder(Order.desc("prodDatePublish"));
		return getByCriteria(index, offset, crit);
	}

	@Override
	public List<Product> findProductByKey(Integer index, Integer offset,
			String key) {
		DetachedCriteria crit = DetachedCriteria
				.forClass(Product.class)
				.createAlias("shop", "sp")
				.createAlias("sp.country", "cnt")
				.createAlias("categorie", "cat")
				.createAlias("sp.owners", "acc")
				.add(Restrictions.or(
						Restrictions.ilike("acc.cptName", key,
								MatchMode.ANYWHERE),
						Restrictions.or(
								Restrictions.ilike("acc.cptFirstname", key,
										MatchMode.ANYWHERE),
								Restrictions.or(
										Restrictions.ilike("acc.cptFirstname",
												key, MatchMode.ANYWHERE),
										Restrictions.or(
												Restrictions.ilike(
														"acc.cptEmail", key,
														MatchMode.ANYWHERE),
												Restrictions.or(
														Restrictions
																.ilike("sp.shopName",
																		key,
																		MatchMode.ANYWHERE),
														Restrictions.or(
																Restrictions
																		.ilike("sp.shopEmail",
																				key,
																				MatchMode.ANYWHERE),
																Restrictions
																		.or(Restrictions
																				.ilike("cat.catLabel",
																						key,
																						MatchMode.ANYWHERE),
																				Restrictions
																						.or(Restrictions
																								.ilike("cnt.cntLabel",
																										key,
																										MatchMode.ANYWHERE),
																								Restrictions
																										.ilike("prodName",
																												key,
																												MatchMode.ANYWHERE))))))))));
		List<Product> res = getByCriteria(crit);
		return res;
	}
}
