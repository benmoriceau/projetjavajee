package com.mti.shop.daoImpl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.stereotype.Repository;

import com.mti.shop.dao.ShopDao;
import com.mti.shop.model.Shop;

@Repository
public class ShopDaoImpl extends GenericDaoImpl<Shop, Long> implements ShopDao {

	public ShopDaoImpl() {
		super();
		this.type = Shop.class;
	}
	
	@Override
	public List<Shop> getAllShop() {
		DetachedCriteria crit = DetachedCriteria.forClass(Shop.class);
		return getByCriteria(crit);
	}

}
