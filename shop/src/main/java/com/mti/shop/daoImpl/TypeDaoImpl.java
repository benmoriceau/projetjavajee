package com.mti.shop.daoImpl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.mti.shop.dao.TypeDao;
import com.mti.shop.model.Type;

@Repository
public class TypeDaoImpl extends GenericDaoImpl<Type, Long> implements TypeDao{
	public TypeDaoImpl() {
		super();
		this.type = Type.class;
	}

	@Override
	public Type findByLabel(String label) {
		DetachedCriteria crit = DetachedCriteria.forClass(Type.class);
		crit.add(Restrictions.eq("typeLabel", label));
		List<Type> res = getByCriteria(crit);
		return res.size() == 0 ? null : res.get(0);
	}
}
