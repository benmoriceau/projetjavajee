package com.mti.shop.service;

import java.util.List;

import com.mti.shop.model.Account;
import com.mti.shop.model.Type;

public interface AccountService {
	void saveOrUpdateAccount(Account acc, Type typeToAdd);
	Account getAccountById(Long id);
	Account getAccountByEmail(String email);
	Account addRole(String role);
	void saveOrUpdateAccount(Account account);
}