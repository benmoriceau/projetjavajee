package com.mti.shop.service;

import java.util.List;

import com.mti.shop.model.Categorie;

public interface CategorieService {
	List<Categorie> findAll();
	Categorie findById(Long id);
	Categorie findByLabel(String label);
}
