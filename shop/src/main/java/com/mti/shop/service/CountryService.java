package com.mti.shop.service;

import java.util.List;

import com.mti.shop.model.Country;

public interface CountryService {
	List<Country> findAll();
	Country findById(Long id);
	Country findByLabel(String label);
}
