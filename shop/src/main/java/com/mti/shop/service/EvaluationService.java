package com.mti.shop.service;

import java.util.List;

import com.mti.shop.model.Evaluation;

public interface EvaluationService {
	void saveOrUpdate(Evaluation evaluation);
	List findBestEvaluatedShop(int index, int offset);
	List<Evaluation> findEvaluationForAccount(int index, int offset, String login);
	List<Evaluation> findEvaluationForShop(int index, int offset, Long shopId);
	List<Evaluation> findEvaluated(int index, int offset, String login);
}
