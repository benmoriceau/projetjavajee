package com.mti.shop.service;

import java.util.List;

import com.mti.shop.model.Product;

public interface ProductService {
	List<Product> getLastProducs(int index, int offset);

	List<Product> getBestEvalProducts(int index, int offset);

	Product getById(Long id);

	void saveOrUpdate(Product product);

	List<Product> findLastProductForShop(Integer index, Integer offset,
			String shopName);

	List<Product> findProductByCategorie(Integer index, Integer offset,
			String categorie);

	List<Product> findExpireSoonProducts(Integer index, Integer offset);

	List<Product> findProductByCountry(Integer index, Integer offset,
			String country);

	List<Product> findProductByKey(Integer index, Integer offset, String key);
}
