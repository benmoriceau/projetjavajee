package com.mti.shop.service;

import java.util.List;

import com.mti.shop.model.Account;
import com.mti.shop.model.Shop;

public interface ShopService {
	List<Shop> getAllShop();

	void addNewShopToCurrentAccount(Shop shop);

	void addShop(Shop shop, Account acc);

	Shop getCurrentShop();

	Shop findById(Long id);
}
