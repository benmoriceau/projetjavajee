package com.mti.shop.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mti.shop.model.Type;


@Service
public interface TypeService {
	Type findByLabel(String label);
	
	List<Type> findAll();
}
