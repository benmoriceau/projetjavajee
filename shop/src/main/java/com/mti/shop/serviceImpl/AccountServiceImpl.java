package com.mti.shop.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mti.shop.dao.AccountDao;
import com.mti.shop.model.Account;
import com.mti.shop.model.Type;
import com.mti.shop.service.AccountService;
import com.mti.shop.tool.GlobalSessionElement;
import com.mti.shop.tool.SessionTool;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountDao accountDao;

	@Autowired
	GlobalSessionElement sessElt;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveOrUpdateAccount(Account account, Type typeToAdd) {
		if (account.getCptPassword() == null
				|| account.getCptPassword().equals("")) {
			// Password not change
			account.setCptPassword(SessionTool.getAccount().getCptPassword());
		}
		if (null != typeToAdd) {
			account.setType(typeToAdd);
		}
		accountDao.saveOrUpdate(account);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveOrUpdateAccount(Account account) {
		accountDao.saveOrUpdate(account);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Account getAccountById(Long id) {
		return accountDao.findById(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Account getAccountByEmail(String email) {
		return accountDao.getAccountByEmail(email);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Account addRole(String role) {
		for (Type type : sessElt.getTypes()) {
			if (role.equals(type.getTypeLabel())) {
				SessionTool.getAccount().setType(type);
				accountDao.saveOrUpdate(SessionTool.getAccount());
				break;
			}
		}
		return SessionTool.getAccount();
	}
}
