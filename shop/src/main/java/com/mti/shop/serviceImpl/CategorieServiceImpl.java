package com.mti.shop.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mti.shop.dao.CategorieDao;
import com.mti.shop.model.Categorie;
import com.mti.shop.service.CategorieService;

@Service
public class CategorieServiceImpl implements CategorieService {

	@Autowired
	CategorieDao categorieDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Categorie> findAll() {
		return categorieDao.findAll();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Categorie findById(Long id) {
		return categorieDao.findById(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Categorie findByLabel(String label) {
		return categorieDao.findByLabel(label);
	}

}
