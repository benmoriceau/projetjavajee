package com.mti.shop.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mti.shop.dao.CountryDao;
import com.mti.shop.model.Country;
import com.mti.shop.service.CountryService;

@Service
public class CountryServiceImpl implements CountryService {

	@Autowired
	CountryDao countryDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Country> findAll() {
		return countryDao.findAll();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Country findById(Long id) {
		return countryDao.findById(id);
	}

	@Override
	public Country findByLabel(String label) {
		return countryDao.findByLabel(label);
	}
}
