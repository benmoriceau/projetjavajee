package com.mti.shop.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mti.shop.dao.EvaluationDao;
import com.mti.shop.model.Evaluation;
import com.mti.shop.service.EvaluationService;

@Service
public class EvaluationServiceImpl implements EvaluationService {

	@Autowired
	EvaluationDao evaluationDao;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveOrUpdate(Evaluation evaluation) {
		evaluationDao.saveOrUpdate(evaluation);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List findBestEvaluatedShop(int index, int offset) {
		List evals = evaluationDao.findBestEvaluatedShop(index, offset);
		
		return evals;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Evaluation> findEvaluationForAccount(int index, int offset,
			String login) {
		return evaluationDao.findEvaluationForAccount(index, offset, login);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Evaluation> findEvaluationForShop(int index, int offset,
			Long shopId) {
		return evaluationDao.findEvaluationForShop(index, offset, shopId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Evaluation> findEvaluated(int index, int offset, String login) {
		return evaluationDao.findEvaluated(index, offset, login);
	}

}
