package com.mti.shop.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mti.shop.dao.ProductDao;
import com.mti.shop.model.Product;
import com.mti.shop.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Product> getLastProducs(int index, int offset) {
		return productDao.getLastProducs(index, offset);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Product> getBestEvalProducts(int index, int offset) {
		return productDao.getBestEvalProducts(index, offset);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Product getById(Long id) {
		return productDao.findById(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveOrUpdate(Product product) {
		productDao.saveOrUpdate(product);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Product> findLastProductForShop(Integer index, Integer offset,
			String shopName) {
		return productDao.findLastProductForShop(index, offset, shopName);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Product> findProductByCategorie(Integer index, Integer offset,
			String categorie) {
		return productDao.findProductByCategorie(index, offset, categorie);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Product> findExpireSoonProducts(Integer index, Integer offset) {
		return productDao.findExpireSoonProducts(index, offset);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Product> findProductByCountry(Integer index, Integer offset,
			String country) {
		return productDao.findProductByCountry(index, offset, country);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Product> findProductByKey(Integer index, Integer offset,
			String key) {
		return productDao.findProductByKey(index, offset, key);
	}
}
