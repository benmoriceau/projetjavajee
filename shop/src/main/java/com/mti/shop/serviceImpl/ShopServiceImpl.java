package com.mti.shop.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mti.shop.dao.ShopDao;
import com.mti.shop.model.Account;
import com.mti.shop.model.Shop;
import com.mti.shop.service.AccountService;
import com.mti.shop.service.ShopService;
import com.mti.shop.tool.SessionTool;

@Service
public class ShopServiceImpl implements ShopService {
	@Autowired
	private ShopDao shopDao;

	@Autowired
	private AccountService accountService;

	@Transactional(propagation = Propagation.REQUIRED)
	public List<Shop> getAllShop() {
		return shopDao.getAllShop();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void addNewShopToCurrentAccount(Shop shop) {
		accountService.addRole("ROLE_SHOP");
		shop.getOwners().add(SessionTool.getAccount());
		shopDao.saveOrUpdate(shop);

		}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Shop getCurrentShop() {
		
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void addShop(Shop shop, Account acc) {
		shop.getOwners().add(acc);
		shopDao.saveOrUpdate(shop);	
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Shop findById(Long id) {
		return shopDao.findById(id);
	}
}
