package com.mti.shop.serviceImpl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.shop.dao.TypeDao;
import com.mti.shop.model.Type;
import com.mti.shop.service.TypeService;

@Service
public class TypeServiceImpl implements TypeService {

	@Autowired
	TypeDao typeDao;

	@Override
	public Type findByLabel(String label) {
		return typeDao.findByLabel(label);
	}

	@Override
	public List<Type> findAll() {
		return typeDao.getByCriteria(DetachedCriteria.forClass(Type.class).add(
				Restrictions.ne("id", 0L)));
	}
}
