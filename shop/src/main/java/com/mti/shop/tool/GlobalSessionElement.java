package com.mti.shop.tool;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mti.shop.model.Country;
import com.mti.shop.model.Type;
import com.mti.shop.service.TypeService;

@Component
public class GlobalSessionElement {
	@Autowired
	TypeService typeService;
	
	private static List<Type> types = null;
	private static List<Country> countries = null;

	public List<Type> getTypes() {
		if (null == types) {
			types = typeService.findAll();
		}
		
		return types;
	}

	public List<Country> getCountries() {
		return countries;
	}
}
