package com.mti.shop.tool;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.mti.shop.model.Account;

public class SessionTool {
	public static Account getAccount() {
		Account res = null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			res = (Account) SecurityContextHolder
					.getContext().getAuthentication().getDetails();
		}
		return res;
	}
}
