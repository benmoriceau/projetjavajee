package com.mti.shop.webservice;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.mti.shop.model.Product;

@WebService
public interface AuthenticationService {

	@WebMethod
	Boolean getAuthentication(@WebParam(name = "login") String login,
			@WebParam(name = "pass") String pass);

	@WebMethod
	List<Product> getMostRecentProduct(@WebParam(name = "index") Integer index,
			@WebParam(name = "offset") Integer offset);
}
