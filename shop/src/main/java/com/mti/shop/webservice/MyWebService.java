package com.mti.shop.webservice;

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.mti.shop.model.Account;
import com.mti.shop.model.Categorie;
import com.mti.shop.model.Country;
import com.mti.shop.model.Evaluation;
import com.mti.shop.model.Product;
import com.mti.shop.model.Shop;

@WebService
public interface MyWebService {


	@WebMethod
	List<Product> getMostRecentProduct(@WebParam(name = "index") Integer index,
			@WebParam(name = "offset") Integer offset);

	@WebMethod
	Account getCurrentAccount();

	@WebMethod
	Boolean createShop(@WebParam(name = "name") String name,
			@WebParam(name = "desc") String desc,
			@WebParam(name = "email") String email,
			@WebParam(name = "country") Long countryId,
			@WebParam(name = "categorie") Long catId,
			@WebParam(name = "accountLogin") String accLog);

	@WebMethod
	Boolean addProduct(@WebParam(name = "prodName") String name,
			@WebParam(name = "prodName") String desc,
			@WebParam(name = "prodDesc") Double price,
			@WebParam(name = "dateExp") Date expire,
			@WebParam(name = "stock") Integer stock,
			@WebParam(name = "categorie") Long cat,
			@WebParam(name = "shopId") Long shop);

	@WebMethod
	List<Product> findLastProductForShop(
			@WebParam(name = "index") Integer index,
			@WebParam(name = "offset") Integer offset,
			@WebParam(name = "shopName") String shopName);

	@WebMethod
	List<Product> findExpireSoonProducts(
			@WebParam(name = "index") Integer index,
			@WebParam(name = "offset") Integer offset);

	@WebMethod
	List<Product> findProductByCategorie(
			@WebParam(name = "index") Integer index,
			@WebParam(name = "offset") Integer offset,
			@WebParam(name = "categorie") String categorie);

	@WebMethod
	List<Product> findProductByCountry(@WebParam(name = "index") Integer index,
			@WebParam(name = "offset") Integer offset,
			@WebParam(name = "country") String country);

	/**
	 * Find by everything: name, cat, cnt, auth..
	 * 
	 * @param index
	 * @param offset
	 * @param key
	 * @return
	 */
	@WebMethod
	List<Product> findProductByKey(@WebParam(name = "index") Integer index,
			@WebParam(name = "offset") Integer offset,
			@WebParam(name = "key") String key);

	@WebMethod
	List<Shop> findBestEvalShop(@WebParam(name = "index") Integer index,
			@WebParam(name = "offset") Integer offset);

	@WebMethod
	List<Categorie> getAllCategories();

	@WebMethod
	List<Country> getAllCountries();

	@WebMethod
	Boolean CreateAccount(@WebParam(name = "name") String name,
			@WebParam(name = "firstname") String firstname,
			@WebParam(name = "phone") String phone,
			@WebParam(name = "addLine1") String addLine1,
			@WebParam(name = "addLine2") String addLine2,
			@WebParam(name = "email") String email,
			@WebParam(name = "pwd") String pwd,
			@WebParam(name = "postCode") String postCode,
			@WebParam(name = "countryLabel") String cntLabel);

	@WebMethod
	Boolean evaluateAccount(@WebParam(name = "evaluator") String Evaluator,
			@WebParam(name = "evaluated") String evaluated,
			@WebParam(name = "note") Integer note,
			@WebParam(name = "comment") String comment);

	@WebMethod
	Boolean evaluateShop(@WebParam(name = "evaluator") String Evaluator,
			@WebParam(name = "evaluated") Long evaluated,
			@WebParam(name = "note") Integer note,
			@WebParam(name = "comment") String comment);
	
	@WebMethod
	List<Evaluation> findEvaluationForAccount(@WebParam(name = "index") Integer index,
			@WebParam(name = "offset") Integer offset,
			@WebParam(name = "login") String login);
	
	@WebMethod
	List<Evaluation> findEvaluationForShop(@WebParam(name = "index") Integer index,
			@WebParam(name = "offset") Integer offset,
			@WebParam(name = "shopId") Long shopId);
	
	@WebMethod
	List<Evaluation> findEvaluated(@WebParam(name = "index") Integer index,
			@WebParam(name = "offset") Integer offset,
			@WebParam(name = "login") String login);

}
