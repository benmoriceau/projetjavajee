package com.mti.shop.webservice;

import java.util.Date;

import com.mti.shop.model.Categorie;
import com.mti.shop.model.Country;
import com.mti.shop.model.Evaluation;
import com.mti.shop.model.Product;
import com.mti.shop.model.Shop;
import com.mti.shop.webserviceImpl.JaxbList;

public interface restService {
	JaxbList<Product> getMostRecentProduct(Integer index, Integer offset);

	String createShop(String name, String desc, String email, Long countryId,
			Long catId, String accLog);

	String addProduct(String name, String desc, Double price, Date expire,
			Integer stock, Long cat, Long shop);

	JaxbList<Product> findLastProductForShop(Integer index, Integer offset,
			String shopName);

	JaxbList<Product> findExpireSoonProducts(Integer index, Integer offset);

	JaxbList<Product> findProductByCategorie(Integer index, Integer offset,
			String categorie);

	JaxbList<Product> findProductByCountry(Integer index, Integer offset,
			String country);

	/**
	 * Find by everything: name, cat, cnt, auth..
	 * 
	 * @param index
	 * @param offset
	 * @param key
	 * @return
	 */
	JaxbList<Product> findProductByKey(Integer index, Integer offset, String key);

	JaxbList<Shop> findBestEvalShop(Integer index, Integer offset);

	JaxbList<Categorie> getAllCategories();

	JaxbList<Country> getAllCountries();

	String CreateAccount(String name, String firstname, String phone,
			String addLine1, String addLine2, String email, String pwd,
			String postCode, String cntLabel);

	String evaluateAccount(String Evaluator, String evaluated, Integer note,
			String comment);

	String evaluateShop(String Evaluator, Long evaluated, Integer note,
			String comment);

	JaxbList<Evaluation> findEvaluationForAccount(int index, int offset,
			String login);

	JaxbList<Evaluation> findEvaluationForShop(int index, int offset,
			Long shopId);

	JaxbList<Evaluation> findEvaluated(int index, int offset, String login);

}
