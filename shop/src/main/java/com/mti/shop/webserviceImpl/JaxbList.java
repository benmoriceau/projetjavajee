package com.mti.shop.webserviceImpl;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.mti.shop.model.Categorie;
import com.mti.shop.model.Country;
import com.mti.shop.model.Evaluation;
import com.mti.shop.model.Product;
import com.mti.shop.model.Shop;

@XmlRootElement(name = "List")
@XmlSeeAlso(value = { Product.class, Shop.class, Evaluation.class,
		Categorie.class, Country.class })
public class JaxbList<T> {
	protected List<T> list;

	public JaxbList() {
	}

	public JaxbList(List<T> list) {
		this.list = list;
	}

	@XmlElement(name = "Item")
	public List<T> getList() {
		return list;
	}
}
