package com.mti.shop.webserviceImpl;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.stereotype.Component;

@Component("serverPasswordMatcherHandler")
public class ServiceAuthentication implements CallbackHandler {

	@Autowired
	@Qualifier(value = "myAuthProvider")
	AuthenticationProvider providerManager;

	@Override
	public void handle(Callback[] arg0) throws IOException,
			UnsupportedCallbackException {
		// WSPasswordCallback pc = null;
		// for (Callback callback : arg0) {
		// if (callback instanceof WSPasswordCallback) {
		// pc = (WSPasswordCallback) callback;
		// break;
		// }
		// }
		// if (providerManager == null)
		// throw new IllegalStateException(
		// "authenticationProvider should've been wired");
		//
		// if (pc != null && StringUtils.hasText(pc.getIdentifier())) {
		//
		// Authentication authentication = providerManager
		// .authenticate(new UsernamePasswordAuthenticationToken(pc
		// .getIdentifier(), "1234"));
		// pc.setPassword(authentication.getCredentials().toString());
		// }
	}

}
