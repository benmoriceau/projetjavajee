package com.mti.shop.webserviceImpl;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mti.shop.model.Categorie;
import com.mti.shop.model.Country;
import com.mti.shop.model.Evaluation;
import com.mti.shop.model.Product;
import com.mti.shop.model.Shop;
import com.mti.shop.webservice.MyWebService;
import com.mti.shop.webservice.restService;

@Service("restService")
@Path("/rest")
@Produces("application/xml")
public class restServiceImpl implements restService {

	@Autowired
	private MyWebService myWebService;

	@Override
	@GET
	@Path("mostRecentProduct/{index}/{offset}")
	public JaxbList<Product> getMostRecentProduct(
			@PathParam("index") Integer index,
			@PathParam("offset") Integer offset) {
		return new JaxbList(myWebService.getMostRecentProduct(index, offset));
	}

	@Override
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("createShop")
	public String createShop(@FormParam("name") String name,
			@FormParam("desc") String desc, @FormParam("email") String email,
			@FormParam("countryId") Long countryId,
			@FormParam("catId") Long catId, @FormParam("accLog") String accLog) {
		return myWebService.createShop(name, desc, email, countryId, catId,
				accLog).toString();
	}

	@Override
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("addProduct")
	public String addProduct(@FormParam("name") String name,
			@FormParam("desc") String desc, @FormParam("price") Double price,
			@FormParam("expire") Date expire,
			@FormParam("stock") Integer stock, @FormParam("catId") Long cat,
			@FormParam("shopId") Long shop) {
		return myWebService.addProduct(name, desc, price, expire, stock, cat,
				shop).toString();
	}

	@Override
	@GET
	@Path("lastProductForShop/{index}/{offset}/{shopName}")
	public JaxbList<Product> findLastProductForShop(
			@PathParam("index") Integer index,
			@PathParam("offset") Integer offset,
			@PathParam("shopName") String shopName) {
		return new JaxbList(myWebService.findLastProductForShop(index, offset,
				shopName));
	}

	@Override
	@GET
	@Path("findExpireSoonProducts/{index}/{offset}/")
	public JaxbList<Product> findExpireSoonProducts(
			@PathParam("index") Integer index,
			@PathParam("offset") Integer offset) {
		return new JaxbList<Product>(myWebService.findExpireSoonProducts(index,
				offset));
	}

	@Override
	@GET
	@Path("findProductByCategorie/{index}/{offset}/{categorie}")
	public JaxbList<Product> findProductByCategorie(
			@PathParam("index") Integer index,
			@PathParam("offset") Integer offset,
			@PathParam("categorie") String categorie) {
		return new JaxbList<Product>(myWebService.findProductByCategorie(index,
				offset, categorie));
	}

	@Override
	@GET
	@Path("findProductByCountry/{index}/{offset}/{country}")
	public JaxbList<Product> findProductByCountry(
			@PathParam("index") Integer index,
			@PathParam("offset") Integer offset,
			@PathParam("country") String country) {
		return new JaxbList<Product>(myWebService.findProductByCountry(index,
				offset, country));
	}

	@Override
	@GET
	@Path("findProductByKey/{index}/{offset}/{key}")
	public JaxbList<Product> findProductByKey(
			@PathParam("index") Integer index,
			@PathParam("offset") Integer offset, @PathParam("key") String key) {
		return new JaxbList<Product>(myWebService.findProductByKey(index,
				offset, key));
	}

	@Override
	@GET
	@Path("findBestEvalShop/{index}/{offset}")
	public JaxbList<Shop> findBestEvalShop(@PathParam("index") Integer index,
			@PathParam("offset") Integer offset) {
		return new JaxbList<Shop>(myWebService.findBestEvalShop(index, offset));
	}

	@Override
	@GET
	@Path("getAllCategories")
	public JaxbList<Categorie> getAllCategories() {
		return new JaxbList<Categorie>(myWebService.getAllCategories());
	}

	@Override
	@GET
	@Path("getAllCountries")
	public JaxbList<Country> getAllCountries() {
		return new JaxbList<Country>(myWebService.getAllCountries());
	}

	@Override
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("createAccount")
	public String CreateAccount(@FormParam("name") String name,
			@FormParam("firstname") String firstname,
			@FormParam("phone") String phone,
			@FormParam("addLine") String addLine1,
			@FormParam("addLine2") String addLine2,
			@FormParam("email") String email, @FormParam("pwd") String pwd,
			@FormParam("postCode") String postCode,
			@FormParam("cntLabel") String cntLabel) {
		return myWebService.CreateAccount(name, firstname, phone, addLine1,
				addLine2, email, pwd, postCode, cntLabel).toString();
	}

	@Override
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("evaluateAccount")
	public String evaluateAccount(@FormParam("evaluator") String Evaluator,
			@FormParam("evaluated") String evaluated,
			@FormParam("note") Integer note,
			@FormParam("comment") String comment) {
		return myWebService
				.evaluateAccount(Evaluator, evaluated, note, comment)
				.toString();
	}

	@Override
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("evaluateShop")
	public String evaluateShop(@FormParam("evaluator") String Evaluator,
			@FormParam("evaluated") Long evaluated,
			@FormParam("note") Integer note,
			@FormParam("comment") String comment) {
		return myWebService.evaluateShop(Evaluator, evaluated, note, comment)
				.toString();
	}

	@Override
	@GET
	@Path("findProductByKey/{index}/{offset}/{login}")
	public JaxbList<Evaluation> findEvaluationForAccount(
			@PathParam("index") int index, @PathParam("offset") int offset,
			@PathParam("login") String login) {
		return new JaxbList<Evaluation>(myWebService.findEvaluationForAccount(
				index, offset, login));
	}

	@Override
	@GET
	@Path("findProductByKey/{index}/{offset}/{key}")
	public JaxbList<Evaluation> findEvaluationForShop(
			@PathParam("index") int index, @PathParam("offset") int offset,
			@PathParam("shopId") Long shopId) {
		return new JaxbList<Evaluation>(myWebService.findEvaluationForShop(
				index, offset, shopId));
	}

	@Override
	@GET
	@Path("findProductByKey/{index}/{offset}/{key}")
	public JaxbList<Evaluation> findEvaluated(@PathParam("index") int index,
			@PathParam("offset") int offset, @PathParam("login") String login) {
		return new JaxbList<Evaluation>(myWebService.findEvaluated(index,
				offset, login));
	}

}
