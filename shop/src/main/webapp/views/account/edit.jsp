<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="../../javascript/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../../all.js"></script>
<title><spring:message code="account.edit" /></title>
</head>
<body>
	<header>
		<jsp:include page="../header.jsp"></jsp:include>
	</header>
	<section>
		<input type="hidden" value="no" id="needToTransformPass">
		<form:form action="saveaccount.htm" modelAttribute="account"
			id="formAddAccount">

			<!-- request for the edit mode -->
			<form:hidden path="id" />

			<spring:message code="account.name" />: <form:input path="cptName" />
			<br />
			<spring:message code="account.firstname" />: <form:input
				path="cptFirstname" />
			<br />
			<spring:message code="account.email" />: <form:input path="cptEmail" />
			<br />
			<spring:message code="account.password" />: <form:password
				id="password" path="cptPassword" onchange="changePassField" />
			<br />
			<spring:message code="account.passwordconfirm" />: <form:password
				id="passwordconfirm" path="cptPasswordConfirm" />
			<br />
			<spring:message code="account.postalcode" />: <form:input
				path="cptPostalCode" />
			<br />
			<spring:message code="account.phonenum" />: <form:input
				path="cptPhoneNum" />
			<br />
			<spring:message code="account.addressline1" />: <form:input
				path="cptAddressLine1" />
			<br />
			<spring:message code="account.addressline2" />: <form:input
				path="cptAddressLine2" />
			<input type="submit" value="OK" />
		</form:form>
	</section>
</body>
</html>