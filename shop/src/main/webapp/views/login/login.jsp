<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><spring:message code="login.logintitle" /></title>
</head>
<body>
	<header>
		<jsp:include page="../header.jsp"></jsp:include>
	</header>
	<section>
		<form name='f' action="<c:url value='j_spring_security_check'/>"
			method='POST'>

			<table>
				<tr>
					<td><spring:message code="login.login" />:</td>
					<td><input type='text' name='j_username' value=''></td>
				</tr>
				<tr>
					<td><spring:message code="login.loginpassword" />:</td>
					<td><input type='password' name='j_password' /></td>
				</tr>
				<tr>
					<td colspan='2'><input name="submit" type="submit"
						value="submit" /></td>
				</tr>
				<tr>
					<td colspan='2'><input name="reset" type="reset" /></td>
				</tr>
			</table>
		</form>
	</section>
</body>
</html>