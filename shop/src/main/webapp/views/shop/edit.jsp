<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><spring:message code="shop.createtitle" /></title>
</head>
<body>
	<header>
		<jsp:include page="../header.jsp"></jsp:include>
	</header>
	<section>
		<div>
			<form:form modelAttribute="shop">
				<spring:message code="shop.name" />: <form:input path="shopName" />
				<br />

				<spring:message code="shop.desc" />: <form:input path="shopDesc" />
				<br />

				<spring:message code="shop.email" />: <form:input path="shopEmail"
					id="emailShop" />
				<br />

				<spring:message code="shop.country" />: 
				<form:select path="cntId" items="${countryList}"
					itemLabel="cntLabel" itemValue="id">
				</form:select>
				<br />

				<spring:message code="shop.categorie" />: 
				<form:select path="catId" items="${categorieList}"
					itemLabel="catLabel" itemValue="id">
				</form:select>
				<br />

				<input type="submit">
				<spring:message code="shop.validate" />
				</input>
			</form:form>
		</div>
	</section>
</body>
</html>