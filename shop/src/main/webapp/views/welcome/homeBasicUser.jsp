<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><spring:message code="welcome.welcometitle"/></title>
</head>
<body>
	<header>
		<jsp:include page="../header.jsp"></jsp:include>
	</header>
	<section>
		<div>
			<spring:message code="welcome.welcome"/> ${account.cptFirstname} ${account.cptName}
		<br/>
			<spring:message code="welcome.listRecentProduct"/>
			<ol>
			</ol>
		<br>
			<spring:message code="welcome.listProductFromBestShop"/>
			<ol>
			</ol>
		</div>
		
	</section>
</body>
</html>